const env = {
  BASE_API_URL: 'https://cat-fact.herokuapp.com',
  DEV:true
};

export default env;
