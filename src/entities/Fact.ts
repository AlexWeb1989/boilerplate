export type MessageStatus ={
  sentCount:number,
  verified:boolean,
  feedback?:string,
}



export default interface Fact {
  _id:string,
  _v:number,
  user:number,
  text:string,
  updatedAt:Date,
  sendDate:Date,
  deleted:boolean,
  source:string,
  type:string,
  status:MessageStatus,
}
