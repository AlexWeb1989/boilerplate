import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { Card } from 'react-native-elements';
import styles from './styles';

interface BookItemProps{
  title: string;
  author?: string;
  coverImageUrl?: string;
  publisher?:string;
  id?: number;
  synopsis?: string;
  onPress: any;
}

const BookItem: React.FC <BookItemProps> = ({
  publisher,coverImageUrl, synopsis, author, title, onPress,
}) => (
  <TouchableOpacity onPress={onPress} style={styles.container}>
    <Card containerStyle={styles.glowContainer}>
      <Card.Title>{title}</Card.Title>
      <View style={styles.titleContainer}>
        <Card.Title>{author}</Card.Title>
      </View>
      <View style={styles.titleContainer}>
        <Text style={styles.description}> publisher {publisher}</Text>
      </View>
      <Card.Divider />
      <Card.Image style={styles.image} source={{ uri: coverImageUrl }}>
        <Text style={styles.descriptionWhite}>
          {synopsis.substr(0, 200)}...
        </Text>
      </Card.Image>
    </Card>
  </TouchableOpacity>
);

export default BookItem;
