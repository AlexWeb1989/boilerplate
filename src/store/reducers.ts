
export const initialState = {
  user: {
    name: undefined,
    password: undefined,
  },
  books:[],
  jwt: undefined,
};

export const reducer = (state, action) => {
  switch (action.type) {
    case 'reset':
      return initialState;
    case 'setUser':
      return { user: action.payload };
    default:
      return state;
  }
};
