import { useMutation, useQuery } from "react-query";


const useQueryMiddleware=(action,method,params)=>{
  if (!params) {
    const { isLoading, isError, data, error } = useQuery(`${action}`, method)

    return {isLoading,isError,data,error}

  }else{

   const  mutationMethod = method.apply(params)

    const { isLoading, isError, data, error } = useMutation(`${action}`, mutationMethod)

    return {isLoading,isError,data,error}
  }
}



export default  useQueryMiddleware;
