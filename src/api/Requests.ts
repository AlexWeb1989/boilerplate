import axios, { AxiosRequestConfig, Method } from 'axios';
import env from '../../env';

const RequestAPI = async (
  method: Method,
  url: string,
  data?: any,
  params?: any,
  jwt?: string,
) => {
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: `Bearer ${jwt}`,
  };

  const config: AxiosRequestConfig = {
    url,
    method,
    baseURL: env.BASE_API_URL,
    headers,
    data,
    params,
  };

  return axios(config);
};

export default RequestAPI;
