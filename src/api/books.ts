import RequestAPI from './Requests';

const getBooks = async () => {
  const url = '/facts/random/?amount=15';
  const body = undefined;
  const res = await RequestAPI('get', url, body, undefined);
  return res;
};

const getBookById = async (id: string) => {
  const url = `/facts/${id}`;
  const body = undefined;
  const { data } = await RequestAPI('get', url, body, undefined);
  return data;
};

const searchBook = async (query: string) => {
  const url = `/facts/random?animal_type=${query}&amount=15`;
  const body = undefined;
  const { data } = await RequestAPI('get', url, body, { q: query });
  return data;
};

export { getBooks, getBookById, searchBook };
