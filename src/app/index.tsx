import React from "react";

import MainNavigatorStack from "../navigation/Router";
import Context from "../store/Context";
import { initialState, reducer } from "../store/reducers";
import useReducerWithMiddleware from "../store/hooks";
import loggerBefore from "../utils/logger";
import { QueryClient, QueryClientProvider } from "react-query";
import useQueryMiddleware from "../api/base/BooksQuery";


const queryClient = new QueryClient()

const Index: React.FC<any> = () => {

  const [store, dispatch] = useReducerWithMiddleware(reducer, initialState,[loggerBefore,useQueryMiddleware]);


  return (
    <QueryClientProvider client={queryClient}>
    <Context.Provider value={{ store, dispatch }}>
      <MainNavigatorStack  />
    </Context.Provider>
    </QueryClientProvider>
  );
};

export default Index;
