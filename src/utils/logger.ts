import env from '../../env';


const loggerBefore = (action, state) => {
  if(env.DEV) {
    console.log('logger before the action:', action, state);
  }
};

export default loggerBefore;
