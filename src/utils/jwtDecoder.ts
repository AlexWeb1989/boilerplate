const parseJwt = (token: string) => {
  const rs = require('jsrsasign');

  const a = token.split('.');
  const pClaim = rs.KJUR.jws.JWS.readSafeJSONString(rs.b64utoutf8(a[1]));

  return pClaim;
};

export { parseJwt };
