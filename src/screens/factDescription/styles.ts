import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'purple',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  bookContainer: {
    height: '90%',
    width: '90%',
    marginTop: '5%',
  },
  scroll: {
    marginTop: '18%',
    flex: 1,
  },
  text: {
    fontSize:20,
    marginHorizontal: '3%',
    textAlign: 'justify',
  },
});

export default styles;
