import React, { useEffect, useState } from "react";
import { ScrollView, Text, View } from "react-native";
import styles from "./styles";
import { getBookById } from "../../api/books";
import Fact from "../../entities/Fact";

type Props = {
  route: any;
};

const Description: React.FC <Props> = ({ route }) => {
  const { id } = route.params;

  // const { store } = useContext<any>(Context);

  const [book, setBook] = useState<Fact>(undefined);

  const fetchBook = async () => {
    const resp = await getBookById(id);

    return resp;
  };

  useEffect(() => {
    const getOneBook = async () => {
      const res = await fetchBook();
      setBook(res);
    };
    getOneBook();
  }, [id]);


  return (
    <View style={styles.container}>
      {book && (
        <ScrollView style={styles.bookContainer}>
          <ScrollView style={styles.scroll}>
            <Text style={styles.text}>{book.text}</Text>
          </ScrollView>
        </ScrollView>
      )}
    </View>
  );
};

export default Description;
