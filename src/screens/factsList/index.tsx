import React, { useContext, useEffect, useState } from 'react';
import {
  ActivityIndicator, FlatList, RefreshControl, View,
} from 'react-native';
// eslint-disable-next-line import/no-unresolved
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input } from 'react-native-elements';
import { StackNavigationProp } from '@react-navigation/stack';
import styles from './styles';
import { StackParams } from '../types';
import { getBooks, searchBook } from '../../api/books';
import Context from '../../store/Context';
import BookItem from '../../components/bookItem';
import Alert from '../../components/Alert';
import Fact from "../../entities/Fact";

type Props = {
  navigation: StackNavigationProp<StackParams>;
};

const FactsList: React.FC <Props> = ({ navigation }) => {
  const [factsList, setFactsList] = useState<Fact[]>(undefined);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [query, setQuery] = useState('');

  const { store } = useContext<any>(Context);

  const fetchBooks = async () => {
    setIsRefreshing(true);
    const resp = await getBooks();
    if (resp) {
      setIsRefreshing(false);
    }
    return resp;
  };

  const fetchFilteredBooks = async () => {
    if (query.length > 0) {
      setIsLoading(true);
      const resp = await searchBook(query.toLowerCase());
      if (resp) {
        setFactsList(resp);
        setIsLoading(false);
      }
      return resp;
    }

    return Alert('What are you trying to find?', 'Seriously');
  };


  useEffect(
    () => navigation.addListener('transitionStart', () => {
      navigation.navigate('FactsList');
    }),
    [navigation],
  );

  useEffect(
    () => {
      const fetchAll = async () => {
        const res = await fetchBooks();
        setFactsList(res.data);
      };
      if (query.length === 0) {
        setTimeout(() => fetchAll(), 1000);
      }
    }, [navigation, query, store],
  );

 console.log('@@status@@',factsList);

  return (
    <View style={styles.container}>

      <View style={styles.searchContainer}>

        <Input
          style={styles.input}
          placeholder="Search"
          onChangeText={(text) => {
            setQuery(text);
          }}
          rightIcon={(
            <Icon
              onPress={() => fetchFilteredBooks()}
              name="search-plus"
              size={24}
              color="#32a852"
            />
          )}
        />
      </View>
      {isLoading && <ActivityIndicator size="large" color="#32a852" />}
      {factsList !== undefined && (
        <FlatList
          style={styles.filtersContainer}
          data={factsList}
          refreshControl={(
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={() => fetchBooks()}
            />
          )}
          keyExtractor={(item) => item._id}
          renderItem={({ item }) => (
            <BookItem
              key={item._id}
              id={+item._id}
              title={item.type}
              publisher={item.source}
              synopsis={item.text}
              coverImageUrl={"https://ca-times.brightspotcdn.com/dims4/default/aad141b/2147483647/strip/true/crop/2048x1452+0+0/resize/840x596!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F74%2F18%2F69fc455714319b86b0fea979b1e5%2Fla-sci-sn-cat-genome-20141107-001"}
              onPress={() => navigation.navigate('Description', { id: item._id })}
            />
          )}
        />
      )}
    </View>
  );
};

export default FactsList;
