import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Platform, StatusBar, View} from 'react-native';

import React from 'react';

import styles from './nav.styles';
import FactsList from '../screens/factsList';
import Description from '../screens/factDescription';

const Stack = createStackNavigator();

export default function MainNavigatorStack() {
  return (
    <NavigationContainer>
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <Stack.Navigator
          initialRouteName={ 'BooksList' }
          screenOptions={{
            headerStyle: {
              backgroundColor: '#32a852',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        >
          <Stack.Screen name="FactList" component={FactsList} options={{ headerLeft: () => null }} />
          <Stack.Screen name="Description" component={Description} />
        </Stack.Navigator>
      </View>
    </NavigationContainer>
  );
}
